package com.jivg.practicafragments

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class WarningDialog:DialogFragment(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable=false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = activity?.let{AlertDialog.Builder(it)}
        builder?.setMessage("Ha ocurrido un error")?.setTitle("Aviso")

        builder?.setPositiveButton("Ok") {
         p0, p1 -> p0?.dismiss()
        }
        return  builder!!.create()
        // return super.onCreateDialog(savedInstanceState)
    }
}