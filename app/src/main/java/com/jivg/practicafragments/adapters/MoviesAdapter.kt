package com.cybertch.recyclerviewexample.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jivg.practicafragments.R
import com.jivg.practicafragments.models.Movie

import kotlinx.android.synthetic.main.item_movie.view.*


class MoviesAdapter (val movies:ArrayList<Movie>, val context: Context?):
    RecyclerView.Adapter<MovieViewHolder>() {

    private var onItemMovieSelectedListener:(movies: Movie)->Unit ={ }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_movie,
                parent,
                false)
        )
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder?.tituloTextView?.text = movie.titulo
        holder?.anioTextView?.text = movie.anio
        holder?.descripcionTextView?.text = movie.descripcion

        if (movie.id % 3 == 1)
            holder?.movieImageView.setImageResource(R.drawable.cementerio)
        else if ( movie.id % 3 == 2)
            holder?.movieImageView.setImageResource(R.drawable.purga)
        else
            holder?.movieImageView.setImageResource(R.drawable.monja)


        // val item = movies[position]
        //context?.let { holder.bind(it) } // Si el contexto no es nulo entonces haz el bind
        holder?.setMovie(movie)



        holder?.setOnItemMovieSelectedListener(onItemMovieSelectedListener)
    }

    public fun setOnItemMovieSelected(listener: (movies: Movie)->Unit){
        this.onItemMovieSelectedListener = listener
    }


    public fun addMovieAtPosition(movies: Movie, position: Int){
        this.movies.add(position ,movies)
        notifyItemInserted(position)
    }
}

class MovieViewHolder(view: View):RecyclerView.ViewHolder(view){
    val movieImageView = view.imageViewPeli
    val tituloTextView = view.textViewTitPelicula
    val anioTextView = view.textViewAnioPelicula
    val descripcionTextView = view.textViewDescripcionPelicula
    private lateinit var movie: Movie

    public fun setMovie(movie: Movie){
        this.movie = movie
    }

    fun setOnItemMovieSelectedListener(listener: (movie: Movie) -> Unit){
        itemView.setOnClickListener{listener(movie)}
    }

}