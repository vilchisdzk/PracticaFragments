package com.jivg.practicafragments.controllers.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jivg.practicafragments.R
import com.jivg.practicafragments.models.Movie

const val idDetailInstance = "DetailInstance"
class DetailActivity : AppCompatActivity() {
    lateinit var movie: Movie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        movie = intent.extras?.getSerializable("movieParam") as Movie


        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add(
                R.id.containerDetail,
                DetailFragment.newInstance(movie)
            ).commit()
        }

    }
}
