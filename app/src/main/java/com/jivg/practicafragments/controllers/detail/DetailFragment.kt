package com.jivg.practicafragments.controllers.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jivg.practicafragments.R
import com.jivg.practicafragments.models.Movie
import kotlinx.android.synthetic.main.fragment_detail.*


private const val ARG_CONTACT_PARAM = "movieParam"

class DetailFragment : Fragment() {
    private var movie: Movie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movie = it.getSerializable(ARG_CONTACT_PARAM) as Movie
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        detTituloTextView.text = movie?.titulo
        detAnioTextView.text = movie?.anio
        detDescripcionTextView.text = movie?.descripcion
        detCalidadTextView.text = movie?.calidad
        detIdiomaTextView.text = movie?.idioma


        val id = movie!!.id
        if (id % 3 == 1)
            detCartelImageView.setImageResource(R.drawable.cementerio)
        else if ( id % 3 == 2)
            detCartelImageView.setImageResource(R.drawable.purga)
        else
            detCartelImageView.setImageResource(R.drawable.monja)

    }


    companion object {
        @JvmStatic
        fun newInstance(movie: Movie) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_CONTACT_PARAM, movie)
                }
            }

    }
}
