package com.jivg.practicafragments.controllers.main

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.jivg.practicafragments.R
import com.jivg.practicafragments.controllers.register.RegisterMovieActivity

import kotlinx.android.synthetic.main.activity_main.*


const val idMainInstance = "MainInstance" // Para cuando se agregue a la pila de fragmentes, este sera el identificador del fragmento metido
const val RESULT_SAVE_MOVIE = 0x2334

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add(
                R.id.containerMain,
                MainFragment.newInstance(),
                idMainInstance       // Para que sea el id del fragmento
            ).commit()
        }




        fab.setOnClickListener { view ->
            val registerMovieIntent = Intent(this,
                RegisterMovieActivity::class.java)
            startActivityForResult(registerMovieIntent,
                RESULT_SAVE_MOVIE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //if(requestCode == RESULT_SAVE_MOVIE) {
            // Trae el fragment y enviale el metodo setActivityResult del Activity
            val mainFragment = supportFragmentManager.findFragmentByTag(idMainInstance)
            mainFragment?.onActivityResult(requestCode,resultCode,data)
        //}

    }
}
