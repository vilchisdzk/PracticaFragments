package com.jivg.practicafragments.controllers.main


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.cybertch.recyclerviewexample.adapter.MoviesAdapter
import com.jivg.practicafragments.controllers.detail.DetailActivity
import com.jivg.practicafragments.controllers.detail.DetailFragment
import com.jivg.practicafragments.R
import com.jivg.practicafragments.models.Movie
import kotlinx.android.synthetic.main.fragment_main.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class MainFragment : Fragment() {
        private lateinit var moviesAdapter: MoviesAdapter

        private var isTablet = false

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            isTablet = resources.getBoolean(R.bool.isTablet)
        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_main, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val linearLayoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

            //val spaceGridLayoutManager // Otro manager, que tambien se utiliza
            //val gridLayoutManager = GridLayoutManager(activity, 2)
            //moviesRecyclerView.layoutManager = gridLayoutManager

            moviesRecyclerView.layoutManager = linearLayoutManager
            moviesRecyclerView.setHasFixedSize(true)

            var movies = getMovies()

            moviesAdapter = MoviesAdapter(movies, context)
            moviesAdapter.setOnItemMovieSelected {
                if (isTablet) {
                    activity?.supportFragmentManager!!
                        .beginTransaction()
                        .replace(
                            R.id.containerDetail,
                            DetailFragment.newInstance(it) // Envia una movie al DetailFragment
                        )
                        .commit()
                } else {
                    val detailIntent = Intent(activity, DetailActivity::class.java)
                    detailIntent.putExtra("movieParam", it)
                    startActivity(detailIntent)
                }
            }
            moviesRecyclerView.adapter = moviesAdapter

        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == RESULT_SAVE_MOVIE){
            val movies = data?.getSerializableExtra("movie") as Movie?
            movies?.let { moviesAdapter.addMovieAtPosition(it,1) }
        }
    }

        companion object {
            @JvmStatic
            fun newInstance() = MainFragment()
        }


    fun getMovies(): ArrayList<Movie> {
        var movies:ArrayList<Movie> = ArrayList()
        movies.add(Movie(1,"Cementerio de Mascotas", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(2,"La Purga", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(3,"La Monja", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))
        movies.add(Movie(4,"Cementerio de Mascotas 2", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(5,"La Purga 2", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(6,"La Monja 2", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))
        movies.add(Movie(7,"Cementerio de Mascotas", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(8,"La Purga", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(9,"La Monja", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))
        movies.add(Movie(10,"Cementerio de Mascotas 2", "HD", "Español", "2019", "El Dr. Louis Creed descubre un cementerio extraño en un bosque cercano a su nueva casa. Cuando el gato de la familia muere atropellado."))
        movies.add(Movie(11,"La Purga 2", "HD", "Español","2019", "La purga solo tiene dos reglas: la primera es que durante esta, los funcionarios del gobierno de \"rango 10\" o superior poseen total inmunidad."))
        movies.add(Movie(12,"La Monja 2", "HD", "Español", "2019", "Una monja se suicida en una abadía rumana y el Vaticano envía a un sacerdote y una novicia a investigar lo sucedido. "))

        return movies
    }
}
