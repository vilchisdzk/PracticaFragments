package com.jivg.practicafragments.controllers.register

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.jivg.practicafragments.R
import com.jivg.practicafragments.controllers.main.idMainInstance

import kotlinx.android.synthetic.main.activity_register_movie.*

const val idRegisterInstance = "RegisterInstance" // Para cuando se agregue a la pila de fragmentes, este sera el identificador del fragmento metido
const val RESULT_REGISTER_MOVIE = 0x2335

class RegisterMovieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_movie)
        setSupportActionBar(toolbar)


        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction().add(
                R.id.containerRegisterMovie,
                RegisterMovieFragment.newInstance(),
                idRegisterInstance
            ).commit()
        }


        registerfab.setOnClickListener { view ->
            val registerFragment = supportFragmentManager.findFragmentByTag(idRegisterInstance) as RegisterMovieFragment
            registerFragment.setOnClickListener(view)
        }
    }

}
