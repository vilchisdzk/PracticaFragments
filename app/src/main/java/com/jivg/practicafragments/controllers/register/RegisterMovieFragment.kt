package com.jivg.practicafragments.controllers.register


import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Toast

import com.jivg.practicafragments.R
import com.jivg.practicafragments.controllers.main.RESULT_SAVE_MOVIE
import com.jivg.practicafragments.models.Movie
import kotlinx.android.synthetic.main.fragment_register_movie.*




class RegisterMovieFragment : Fragment() {

    var condiciones = false
    lateinit var calidad:String
    lateinit var idiomaSeleccionado:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        regTrailerimageButton.setImageResource(R.drawable.ic_movie)

        regTituloTextInputLayout.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0.isNullOrEmpty() ){
                    regTituloTextInputLayout.isErrorEnabled = false


                }else{
                    regTituloTextInputLayout.isErrorEnabled = true
                    regTituloTextInputLayout.error = "No puede estar vacio este campo"
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        regDescripcionTextInputLayout.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0.isNullOrEmpty() ){
                    regDescripcionTextInputLayout.isErrorEnabled = false

                }else{
                    regDescripcionTextInputLayout.isErrorEnabled = true
                    regDescripcionTextInputLayout.error = "No puede estar vacio este campo"
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


        regAnioTextInputLayout.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0.isNullOrEmpty() ){
                    regAnioTextInputLayout.isErrorEnabled = false
                }else{
                    regAnioTextInputLayout.isErrorEnabled = true
                    regAnioTextInputLayout.error = "No puede estar vacio este campo"
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


        idiomaSeleccionado = "Español"
        regIdiomaRadioGroup.setOnCheckedChangeListener{ group, checkedId ->
            when(checkedId){
                regEspaniolRadioButton.id ->
                    idiomaSeleccionado = "Español"
                regSubtituladoRadioButton.id ->
                    idiomaSeleccionado = "Subtitulado"
            }
        }

        val tiposCalidad = arrayListOf<String>("SD", "HD", "3D", "4K")
        val calidadAdapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_spinner_item, tiposCalidad)
        calidadAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // Ocupa una vista por default
        regCalidadspinner.adapter=calidadAdapter

        regCalidadspinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val calidadSeleccionada = tiposCalidad[position]
              //  hashMap.put("calidad", calidadSeleccionada)
                calidad = calidadSeleccionada
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })

        regTerminosCheckBox.setOnCheckedChangeListener(onCheckedChangeListener)

        regTrailerimageButton.setOnClickListener(View.OnClickListener {
            val id = "t-eSNuv28XQ"
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id")
            )
            try {
                startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                startActivity(webIntent)
            }

        })

    }

    public fun setOnClickListener(view:View){

        if(condiciones) {
            if(regTituloTextInputLayout.editText?.text.toString() != "" && regDescripcionTextInputLayout.editText?.text.toString() != "" && regAnioTextInputLayout.editText?.text.toString() != "") {
                val movie = Movie(
                    1,
                    regTituloTextInputLayout.editText?.text.toString(),
                    calidad,
                    idiomaSeleccionado,
                    regAnioTextInputLayout.editText?.text.toString(),
                    regDescripcionTextInputLayout.editText?.text.toString()
                )
                val intent = Intent()
                intent.putExtra("movie" ,movie)
                activity?.setResult(RESULT_SAVE_MOVIE, intent)
                activity?.finish()
            }else{
                Toast.makeText(activity!!,"Debe llenar todos los campos",Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(activity!!,"Debe aceptar los terminos y condiciones",Toast.LENGTH_LONG).show()

        }


    }


    companion object {

        @JvmStatic
        fun newInstance() = RegisterMovieFragment()

    }

    private val onCheckedChangeListener: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            condiciones = isChecked }
}
