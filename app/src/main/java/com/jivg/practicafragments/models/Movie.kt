package com.jivg.practicafragments.models

import java.io.Serializable

data class Movie (
    var id:Int,
    var titulo:String,
    var calidad:String,
    var idioma:String,
    var anio:String,
    var descripcion:String
):Serializable